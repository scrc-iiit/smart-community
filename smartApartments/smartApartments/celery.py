from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'smartApartments.settings')

app = Celery('smartApartments')

app.conf.enable_utc = False
app.config_from_object("django.conf:settings", namespace="CELERY")

# Celery beat settings
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
