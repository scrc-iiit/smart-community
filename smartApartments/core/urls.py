from django.urls import include, path
from rest_framework import routers

from core.viewsets import (LabelViewset,
                           ActuatorViewset,
                           SensorViewset,
                           DeviceViewset,
                           NodeViewset,
                           Onem2mInstanceViewset,
                           CollectionViewset,)

router = routers.DefaultRouter()
router.register(r'label', LabelViewset)
router.register(r'actuator', ActuatorViewset)
router.register(r'sensor', SensorViewset)
router.register(r'device', DeviceViewset)
router.register(r'node', NodeViewset)
router.register(r'onem2minstance', Onem2mInstanceViewset)
router.register(r'collection', CollectionViewset)

urlpatterns = [
    path('', include(router.urls)),
]
