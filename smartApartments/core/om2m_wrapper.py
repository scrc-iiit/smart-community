import requests
import os
import json
import sys


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def cprint(text, color=bcolors.OKBLUE, **kwargs):
    print(f"{color}{text}{bcolors.ENDC}", **kwargs)


OM2M_USERNAME = os.getenv('OM2M_USERNAME', 'admin')
OM2M_PASSWORD = os.getenv('OM2M_PASSWORD', 'admin')
OM2M_DEFAULT_MNI = int(os.getenv('OM2M_DEFAULT_MNI', '12000'))

resource_types = [
    'accessControlPolicy',
    'AE',
    'container',
    'contentInstance',
    'CSEBase',
    'delivery',
    'eventConfig',
    'execInstance',
    'Group',
    'fanOutPoint',
    'localPolicy',
    'm2mServiceSubscription',
    'mgmtCmd',
    'mgmtObj',
    'Node',
    'nodeInfo',
    'pollingChannel',
    'remoteCSE',
    'Request',
    'Schedule',
    'statsCollect',
    'statsConfig',
    'Subscription'
]


def get_resource_type(_type):
    try:
        return resource_types.index(_type) + 1
    except ValueError as e:
        cprint(
            f"Wrong resource type specified {_type} using contentInstance as default", bcolors.WARNING)
        return resource_types.index('contentInstance') + 1


def check_format(format):
    if format == 'json' or format == 'xml':
        return format
    else:
        cprint(
            f"Wrong format type specified {format} using json as default", bcolors.WARNING)


def resource_type_parser(_type="contentInstance", resource_name=None, **kwargs):
    _action = kwargs.get("action", "create")
    _resource_type = get_resource_type(_type)
    _keys = kwargs.keys()
    if "labels" not in _keys:
        cprint("labels not specified leaving it blank", bcolors.WARNING)
    if (not resource_name) and (_resource_type != 4):
        if _action == 'create':
            raise AttributeError(
                "resource_name not specified. resource_name is required attribute")
        elif _action == 'update':
            cprint(
                    "resource_name attribute not permitted ignoring resource_name", bcolors.WARNING)
    if _resource_type == 2:
        if _action == 'create':
            if "application_id" not in _keys:
                cprint(
                    "application_id attribute not specified using defaults", bcolors.WARNING)
            if "request_reachability" not in _keys:
                cprint(
                    "request_reachability attribute not specified using defaults", bcolors.WARNING)
        elif _action == 'update':
            if "application_id" in _keys:
                cprint(
                    "application_id attribute not permitted ignoring application_id", bcolors.WARNING)
        return _resource_type, 'ae'
    elif _resource_type == 3:
        return _resource_type, 'cnt'
    elif _resource_type == 4:
        if _action == 'update':
            raise Exception(
                "container instances cannot be updated")
        if "content" not in _keys:
            raise AttributeError(
                "content attribute not specified. content is required attribute")
        return _resource_type, 'cin'
    elif _resource_type == 9:
        if _action == 'create':
            if "member_ids_list" not in _keys:
                raise AttributeError(
                    "member_ids_list attribute not specified. member_ids_list is required attribute")
            if "max_members" not in _keys:
                raise AttributeError(
                    "max_members attribute not specified. member_ids_list is required attribute")
        return _resource_type, 'grp'
    else:
        raise NotImplementedError("Other Types are not supported yet ..")


def retrieve_resource(cse_endpoint, **kwargs):
    _verify = kwargs.get('verify', False)
    _data_format = check_format(kwargs.get('data_format', 'json'))
    headers = {
        'X-M2M-Origin': f'{OM2M_USERNAME}:{OM2M_PASSWORD}',
        'Content-type': f'application/{_data_format}; charset=utf-8'
    }
    try:
        response = requests.get(cse_endpoint, headers=headers, verify=_verify)
        response.raise_for_status()
        response_obj = json.loads(response.text)
        return response_obj[next(iter(response_obj))]
    except requests.exceptions.HTTPError as e:
        cprint(e.response.text, bcolors.WARNING)
    except requests.exceptions.Timeout:
        cprint(
            "Timeout: server did not respond with request in stipulated time", bcolors.FAIL)
    except requests.exceptions.ConnectionError:
        cprint("ConnectionError: check cse_endpoint", bcolors.FAIL)
    except requests.exceptions.RequestException as e:
        cprint(" ERROR ".center(80, "-"), bcolors.FAIL)
        cprint(e, bcolors.FAIL, file=sys.stderr)
    return False


def create_resource(cse_endpoint, resource_name=None, resource_type="container", **kwargs):
    cprint(f" Creating {resource_type} in {cse_endpoint} ".center(100, "-"))
    _type = resource_type_parser(resource_type, resource_name, **kwargs)
    _content = kwargs.get('content', None)
    _application_id = kwargs.get('application_id', resource_name)
    _access_control_policy_id = kwargs.get('access_control_policy_id', None)
    _labels = kwargs.get('labels', [])
    _verify = kwargs.get('verify', False)
    _mni = kwargs.get('mni', OM2M_DEFAULT_MNI)
    _request_reachability = kwargs.get('request_reachability', True)
    _data_format = check_format(kwargs.get('data_format', 'json'))
    _member_ids_list = kwargs.get('member_ids_list', [])
    _max_members = kwargs.get('max_members', None)
    headers = {
        'X-M2M-Origin': f'{OM2M_USERNAME}:{OM2M_PASSWORD}',
        'Content-type': f'application/{_data_format}; charset=utf-8; ty={_type[0]}'
    }
    _data = {
        "rr": _request_reachability,
        "lbl": _labels,
        "mni": _mni,
    }
    if _application_id:
        _data['api'] = _application_id
    if _access_control_policy_id:
        _data['acpi'] = _access_control_policy_id
    if resource_name:
        _data['rn'] = resource_name
    if _content:
        _data['con'] = _content
    if _member_ids_list:
        _data['mid'] = _member_ids_list
    if _max_members:
        _data['mnm'] = _max_members


    payload = {
        f"m2m:{_type[1]}": {
            **_data
        }
    }
    try:
        response = requests.post(cse_endpoint, json=payload,
                                 headers=headers, verify=_verify)
        response.raise_for_status()
        return response.ok
    except requests.exceptions.HTTPError as e:
        cprint(e.response.text, bcolors.WARNING)
    except requests.exceptions.Timeout:
        cprint(
            "Timeout: server did not respond with request in stipulated time", bcolors.FAIL)
    except requests.exceptions.ConnectionError:
        cprint("ConnectionError: check cse_endpoint", bcolors.FAIL)
    except requests.exceptions.RequestException as e:
        cprint(" ERROR ".center(80, "-"), bcolors.FAIL)
        cprint(e, bcolors.FAIL, file=sys.stderr)
    return False


def delete_resource(cse_endpoint, **kwargs):
    _verify = kwargs.get('verify', False)
    _data_format = check_format(kwargs.get('data_format', 'json'))
    headers = {
        'X-M2M-Origin': f'{OM2M_USERNAME}:{OM2M_PASSWORD}',
        'Content-type': f'application/{_data_format}; charset=utf-8'
    }
    try:
        response = requests.delete(
            cse_endpoint, headers=headers, verify=_verify)
        response.raise_for_status()
        return response.ok
    except requests.exceptions.HTTPError as e:
        cprint(e.response.text, bcolors.WARNING)
    except requests.exceptions.Timeout:
        cprint(
            "Timeout: server did not respond with request in stipulated time", bcolors.FAIL)
    except requests.exceptions.ConnectionError:
        cprint("ConnectionError: check cse_endpoint", bcolors.FAIL)
    except requests.exceptions.RequestException as e:
        cprint(" ERROR ".center(80, "-"), bcolors.FAIL)
        cprint(e, bcolors.FAIL, file=sys.stderr)
    return False


def update_resource(cse_endpoint, resource_name=None, resource_type="container", **kwargs):
    cprint(f" Updating {resource_type} in {cse_endpoint} ".center(100, "-"))
    _type = resource_type_parser(resource_type, resource_name, action="update",**kwargs)
    _access_control_policy_id = kwargs.get('access_control_policy_id', None)
    _labels = kwargs.get('labels', [])
    _verify = kwargs.get('verify', False)
    _mni = kwargs.get('mni', OM2M_DEFAULT_MNI)
    _request_reachability = kwargs.get('request_reachability', True)
    _data_format = check_format(kwargs.get('data_format', 'json'))
    _member_ids_list = kwargs.get('member_ids_list', [])
    _max_members = kwargs.get('max_members', None)
    headers = {
        'X-M2M-Origin': f'{OM2M_USERNAME}:{OM2M_PASSWORD}',
        'Content-type': f'application/{_data_format}; charset=utf-8; ty={_type[0]}'
    }
    _data = {
        "rr": _request_reachability,
        "lbl": _labels,
        "mni": _mni,
    }
    if _access_control_policy_id:
        _data['acpi'] = _access_control_policy_id
    if _member_ids_list:
        _data['mid'] = _member_ids_list
    if _max_members:
        _data['mnm'] = _max_members


    payload = {
        f"m2m:{_type[1]}": {
            **_data
        }
    }

    try:
        response = requests.put(cse_endpoint, json=payload,
                                 headers=headers, verify=_verify)
        response.raise_for_status()
        return response.ok
    except requests.exceptions.HTTPError as e:
        cprint(e.response.text, bcolors.WARNING)
    except requests.exceptions.Timeout:
        cprint(
            "Timeout: server did not respond with request in stipulated time", bcolors.FAIL)
    except requests.exceptions.ConnectionError:
        cprint("ConnectionError: check cse_endpoint", bcolors.FAIL)
    except requests.exceptions.RequestException as e:
        cprint(" ERROR ".center(80, "-"), bcolors.FAIL)
        cprint(e, bcolors.FAIL, file=sys.stderr)
    return False

if __name__ == '__main__':
    print(update_resource(
        "http://localhost:8080/~/in-cse/in-name/SCRIPT-AE", None, 'AE',labels=["EST"]))
