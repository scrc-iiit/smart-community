from rest_framework import viewsets

from core.models import (Label,
                         Actuator,
                         Sensor,
                         Device,
                         Node,
                         Onem2mInstance,
                         Collection)

from core.serializers import (LabelSerializer,
                              ActuatorSerializer,
                              SensorSerializer,
                              DeviceSerializer,
                              NodeSerializer,
                              Onem2mInstanceSerializer,
                              CollectionSerializer)

# Create your viewsets here.


class LabelViewset(viewsets.ModelViewSet):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer


class ActuatorViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Actuator.objects.all()
    serializer_class = ActuatorSerializer


class SensorViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer

class DeviceViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

class NodeViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Node.objects.all()
    serializer_class = NodeSerializer


class Onem2mInstanceViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Onem2mInstance.objects.all()
    serializer_class = Onem2mInstanceSerializer


class CollectionViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer