from __future__ import absolute_import, unicode_literals
import sys
import json
import requests
import posixpath
from urllib.parse import urljoin, urlparse, parse_qs
from .utils import *
import random
import string


class OneM2MClient:
    def __init__(self, cse_endpoint, credentials={}, max_instance_count=0, data_format='json', verify=False) -> None:
        parsed = urlparse(cse_endpoint)
        self.cse_base = parsed.path
        self.http_secure = True if parsed.scheme == "https" else False
        self.verify = verify
        self.url = urljoin(cse_endpoint, parsed.path)
        self.data_format = check_format(data_format)
        if not credentials:
            credentials['OM2M_USERNAME'] = OM2M_USERNAME
            credentials['OM2M_PASSWORD'] = OM2M_PASSWORD
        self.max_instance_count = OM2M_DEFAULT_MNI if not max_instance_count else max_instance_count
        self.credentials = credentials

    def get_headers(self, resource_char):
        return {
            'X-M2M-Origin': f"{self.credentials['OM2M_USERNAME']}:{self.credentials['OM2M_PASSWORD']}",
            'Content-type': f'application/{self.data_format}; charset=utf-8; ty={resource_char}'
        }

    def get_url(self, action, relative_path="", resource_name=""):
        if action is Action.create:
            path = posixpath.join(self.cse_base, relative_path)
        else:
            path = posixpath.join(self.cse_base, relative_path, resource_name)
        return urljoin(self.url, path)

    def verify_data(self, action=Action.create, resource_type=ResourceType.container, **kwargs):
        required = []
        default_value_keys = []
        not_required = []

        if resource_type is ResourceType.applicationEntity:
            if action is Action.create:
                required = ['resource_name']
                default_value_keys = [
                    'application_id',
                    'request_reachability',
                    'access_control_policy_id']
            elif action is Action.update:
                not_required = ['application_id', 'resource_name']

        elif resource_type is ResourceType.contentInstance:
            if action is Action.create:
                required = ['content']
                default_value_keys = ['max_instance_count']
            elif action is Action.update:
                raise Exception("container instances cannot be updated")

        elif resource_type is ResourceType.group:
            if action is Action.create:
                # ? Check What max members is
                required = ['member_ids_list', 'max_members']
            elif action is Action.update:
                not_required = ['resource_name']
            elif action is Action.destroy:
                pass

        elif resource_type is ResourceType.subscription:
            if action is Action.create:
                required = ['nu']  # ? Check What nu members is
            elif action is Action.update:
                not_required = ['resource_name']
        check_defaults(required, default_value_keys, not_required, kwargs)
        return True

    def get_payload(self, action, resource_num, kwargs):
        _labels = kwargs.get('labels', [])
        _member_ids_list = kwargs.get('member_ids_list', [])
        _max_instance_count = kwargs.get('max_instance_count', None)
        _request_reachability = kwargs.get('request_reachability', None)

        _resource_name = kwargs.get('resource_name', None)
        _application_id = kwargs.get("application_id", None)
        _access_control_policy_id = kwargs.get(
            'access_control_policy_id', None)
        _max_members = kwargs.get('max_members', None)
        _content = kwargs.get('content', None)
        _nu = kwargs.get('nu', None)
        _nct = kwargs.get('nct', None)

        _data = {}
        if action is Action.update:
            if _request_reachability:
                _data['rr'] = _request_reachability
            if _max_instance_count:
                _data['mni'] = _max_instance_count
        if action is Action.create:
            _data['rr'] = _request_reachability if _request_reachability else True
            _data['mni'] = _max_instance_count if _max_instance_count else self.max_instance_count
            if _resource_name:
                _data['rn'] = _resource_name
            if _application_id:
                _data['api'] = _application_id
        if _content:
            _data['con'] = _content
        if _access_control_policy_id:
            _data['acpi'] = _access_control_policy_id
        if _member_ids_list:
            _data['mid'] = _member_ids_list
        if _max_members:
            _data['mnm'] = _max_members
        if _labels:
            _data['lbl'] = _labels
        if _nu:
            _data['nu'] = _nu
        if _nct:
            _data['nct'] = _nct
        payload = {
            f"m2m:{resource_num}": {
                **_data
            }
        }
        return payload

    def manage_resource(self, relative_path="", action=Action.create, resource_type=ResourceType.container, **kwargs):
        cprint(f"{action} {resource_type} in {self.cse_endpoint} ".center(100, "-"))
        _resource_name = kwargs.get('resource_name', None)
        if action == Action.update:
            _resource_name = kwargs.pop('resource_name', None)
        self.verify_data(action, resource_type, **kwargs)
        _verify = kwargs.get('verify', False)
        _headers = self.get_headers(resource_type.value[0])
        _payload = self.get_payload(action, resource_type.value[1], kwargs)
        _sender = getattr(requests, action.value)
        _url = self.get_url(action, relative_path, _resource_name)
        # print(_headers, _payload, _url)
        try:
            response = _sender(_url, verify=_verify, json=_payload,
                               headers=_headers)
            response.raise_for_status()
            cprint(f"Success {action} {resource_type} in {self.cse_endpoint}".center(
                100, "-"), bcolors.OKGREEN)
            if action is Action.read:
                response_obj = json.loads(response.text)
                return response_obj[next(iter(response_obj))]
            return response.ok
        except requests.exceptions.HTTPError as e:
            cprint(e.response.text, bcolors.WARNING)
        except requests.exceptions.Timeout:
            cprint(
                "Timeout: server did not respond with request in stipulated time", bcolors.FAIL)
        except requests.exceptions.ConnectionError:
            cprint("ConnectionError: check cse_endpoint", bcolors.FAIL)
        except requests.exceptions.RequestException as e:
            cprint(" ERROR ".center(80, "-"), bcolors.FAIL)
            cprint(e, bcolors.FAIL, file=sys.stderr)
        return False

    def verify_connection(self):
        raise NotImplementedError("Will Do later")


if __name__ == '__main__':
    sample_resource_name = ''.join(random.choices(string.ascii_letters, k=7))
    client = OneM2MClient("http://localhost:8080/~/in-cse/in-name?fu=1&ty=2")
    print(client.manage_resource(action=Action.read))
    # client.manage_resource(resource_name=sample_resource_name)
    # client.manage_resource(action=Action.update,resource_name=sample_resource_name, labels=['TEST'])
    # assert client.manage_resource(action=Action.read,resource_name=sample_resource_name)['lbl'] == ['TEST']
    # client.manage_resource(action=Action.delete, resource_name=sample_resource_name)
    # assert client.manage_resource(action=Action.read,resource_name=sample_resource_name) == False
