import os
import typing
import enum

OM2M_USERNAME = os.getenv('OM2M_USERNAME', 'admin')
OM2M_PASSWORD = os.getenv('OM2M_PASSWORD', 'admin')
OM2M_DEFAULT_MNI = int(os.getenv('OM2M_DEFAULT_MNI', '12000'))


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ResourceType(enum.Enum):
    accessControlPolicy = (1, 'acp')
    applicationEntity = (2, 'ae')
    container = (3, 'cnt')
    contentInstance = (4, 'cin')
    cseBase = (5, 'cb')
    group = (9, 'grp')
    subscription = (23, 'sub')


class Action(enum.Enum):
    create = 'post'
    read = 'get'
    update = 'put'
    destroy = delete = 'delete'
    notify = 'post'

def cprint(text, color=bcolors.OKBLUE, **kwargs):
    print(f"{color}{text}{bcolors.ENDC}", **kwargs)


def check_format(format):
    if format == 'json' or format == 'xml':
        return format
    else:
        cprint(
            f"Wrong format type specified {format} using json as default", bcolors.WARNING)


def check_defaults(required: typing.List = [], default_value_keys: typing.List = [], not_required: typing.List = [], object=None):
    if not object:
        return
    for item in required:
        if item not in object:
            raise AttributeError(
                f"{item} not specified. {item} is required attribute")
    for item in default_value_keys:
        if item not in object:
            cprint(
                f"{item} attribute not specified using defaults", bcolors.WARNING)
    for item in not_required:
        if item in object:
            cprint(
                f"{item} attribute specified Cannot perform action for {item} skipping value", bcolors.WARNING)
