from django.db.models.signals import post_save, m2m_changed, pre_save, pre_delete
from django.dispatch import receiver

from .models import *
# from . import tree
from . import om2m_wrapper as om2m


def create_nodes(instance):
    _onem2m = instance.collection.om2m_instance
    _collection = instance.collection
    _data = {
        "verify": _onem2m.verify,
        "access_control_policy_id": _collection.access_control_id,
        "labels": list(instance.label.values_list('item', flat=True)),
        "mni": _collection.max_instance_count
    }
    # Create Node
    om2m.create_resource(
        f"{_onem2m.link}{_collection.name}",
        instance.name,
        'container',
        **_data
    )
    # Create Data Container in Node
    _data.pop('labels')
    om2m.create_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}",
        "Data",
        'container',
        **_data,
        labels=['Data']
    )
    # Create Descriptor Container in Node
    om2m.create_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}",
        "Descriptor",
        'container',
        **_data,
        labels=['Descriptor']
    )
    # Create Descriptor instance
    # _content = core_serializers.DescriptorSerializer(_descriptor).data
    # _content = descriptor_parser(_content)
    om2m.create_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}/Descriptor",
        None,
        'contentInstance',
        **_data,
        labels=['Descriptor'],
        content="Sample conten, with comma"
    )


def delete_nodes(instance):
    _onem2m = instance.collection.om2m_instance
    _collection = instance.collection
    om2m.delete_resource(f"{_onem2m.link}{_collection.name}/{instance.name}")


def update_nodes(instance):
    _onem2m = instance.collection.om2m_instance
    _collection = instance.collection
    _data = {
        "verify": _onem2m.verify,
        "access_control_policy_id": _collection.access_control_id,
        "labels": list(instance.label.values_list('item', flat=True)),
        "mni": _collection.max_instance_count
    }
    # Update Node
    om2m.update_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}",
        None,
        'container',
        **_data
    )
    # Create Descriptor Container in Node
    _data.pop('labels')
    om2m.update_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}/Descriptor",
        None,
        'container',
        **_data,
    )
    # Create Descriptor instance
    # _content = core_serializers.DescriptorSerializer(_descriptor).data
    # _content = descriptor_parser(_content)
    om2m.create_resource(
        f"{_onem2m.link}{_collection.name}/{instance.name}/Descriptor",
        None,
        'contentInstance',
        **_data,
        content="Changed Strings"
    )


@receiver(post_save, sender=Collection)
def create_collection(sender, instance, created, **kwargs):
    _onem2m = instance.om2m_instance
    _data = {
        "verify": _onem2m.verify,
        "access_control_policy_id": instance.access_control_id,
        "labels": list(instance.label.values_list('item', flat=True)),
        "mni": instance.max_instance_count
    }
    if created:
        if om2m.create_resource(f"{_onem2m.link}", instance.name, 'AE', **_data):
            print("Creation Successful")
    else:
        if om2m.update_resource(f"{_onem2m.link}{instance.name}", None, 'AE', **_data):
            print("Updation Successful")


@receiver(pre_delete, sender=Collection)
def delete_vertical(sender, instance, **kwargs):
    _onem2m = instance.om2m_instance
    if om2m.delete_resource(f"{_onem2m.link}{instance.name}"):
        print("Deletion Successful")


@receiver([post_save, m2m_changed], sender=Node)
def create_node(sender, instance, created, **kwargs):
    if created:
        create_nodes(instance)
    else:
        update_nodes(instance)


@receiver(pre_delete, sender=Node)
def delete_node(sender, instance, **kwargs):
    delete_nodes(instance)
