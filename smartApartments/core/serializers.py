from rest_framework import serializers
from core.models import (Label,
                         Actuator,
                         Sensor,
                         Device,
                         Node,
                         Onem2mInstance,
                         Collection)

class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        exclude=['id',]


class ActuatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actuator
        fields = '__all__'


class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = '__all__'


class DeviceSerializer(serializers.ModelSerializer):
    sensors = SensorSerializer(many=True)
    class Meta:
        model = Device
        fields = '__all__'


class NodeSerializer(serializers.ModelSerializer):
    label = LabelSerializer(many=True)
    device = DeviceSerializer()
    class Meta:
        model = Node
        fields = '__all__'


class Onem2mInstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Onem2mInstance
        fields = '__all__'


class CollectionSerializer(serializers.ModelSerializer):
    label = LabelSerializer(many=True)
    class Meta:
        model = Collection
        fields = '__all__'
