from django.contrib import admin, messages
from django.apps import apps
from django.contrib.admin.decorators import register
from django.utils.translation import ngettext
from .models import *
from .om2m import OneM2MClient, Action, ResourceType

# Register your models here.

@register(Onem2mInstance)
class Onem2mInstanceAdmin(admin.ModelAdmin):
    actions = ['get_collections']

    @admin.action(description='Get Collections From Selected instances')
    def get_collections(self, request, queryset):
        updated = queryset.update()
        for item in queryset:
            client = OneM2MClient(item.link)
            client.manage_resource(action=Action.update)
        self.message_user(request, ngettext(
            '%d story was successfully marked as published.',
            '%d stories were successfully marked as published.',
            updated,
        ) % updated, messages.SUCCESS)

@register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    filter_horizontal = ['label', ]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['name']
        return []

    def get_collection(self, request, queryset):
        updated = queryset.update(status='p')
        self.message_user(request, ngettext(
            '%d story was successfully marked as published.',
            '%d stories were successfully marked as published.',
            updated,
        ) % updated, messages.SUCCESS)


@register(Device)
class DeviceAdmin(admin.ModelAdmin):
    filter_horizontal = ['sensors', 'actuators']


@register(Node)
class NodeAdmin(admin.ModelAdmin):
    filter_horizontal = ['label', ]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['name']
        return []


models = apps.get_models(include_auto_created=False, include_swapped=False)
for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
