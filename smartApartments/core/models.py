import re
from django.db import models
from django.core.validators import RegexValidator
from location_field.models.plain import PlainLocationField
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


# Create your models here.


class repr_decorator:
    def __init__(self, function, item=None):
        self.function = function
        self.item = item

    def __str__(self):
        return f"{self.value}"

    def __unicode__(self):
        return f"{self.value}"

    def __repr__(self):
        return f"{self.value}"

    def __call__(self, *args, **kwargs):
        # before function
        self.value = getattr(self.function, self.item, None)
        if self.value:
            setattr(self.function, "__str__", self.__str__)
            setattr(self.function, "__unicode__", self.__unicode__)
            setattr(self.function, "__repr__", self.__repr__)
        # after function
        return self.function(*args, **kwargs)


def validate_name(value):
    print(type(value))
    if re.search(r'[^\w\-~#.]', value):
        raise ValidationError(
            _('%(value)s is not Alphanumeric or "_", "-", "~", "#"'),
            params={'value': value},
        )


class Label(models.Model):
    item = models.CharField(max_length=256, unique=True)

    def __str__(self):
        return f"{self.item}"

    def __unicode__(self):
        return f"{self.item}"

    def __repr__(self):
        return f"{self.item}"


class Actuator(models.Model):
    name = models.CharField(max_length=512, unique=True)
    description = models.TextField(blank=True, null=True)
    boolean = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name} - {self.id}"

    def __unicode__(self):
        return f"{self.name} - {self.id}"

    def __repr__(self):
        return f"{self.name} - {self.id}"


class Sensor(models.Model):
    name = models.CharField(max_length=512)
    description = models.TextField(blank=True, null=True)
    custom_id = models.DecimalField(
        default=1.0, decimal_places=2, max_digits=5, blank=True, null=True)

    model = models.CharField(max_length=512, blank=True, null=True)
    data_type = models.CharField(max_length=512, blank=True, null=True)
    units = models.CharField(max_length=512, blank=True, null=True)
    resolution = models.CharField(max_length=512, blank=True, null=True)
    accuracy = models.DecimalField(
        max_digits=5, decimal_places=2, default=80.0)
    data_label = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return f"{self.name} - {self.model}"

    def __unicode__(self):
        return f"{self.name} - {self.model}"

    def __repr__(self):
        return f"{self.name} - {self.model}"

    @property
    def repr(self):
        return f"({self.name}{f' = {self.description}' if self.description else ''}{f',id={self.custom_id}' if self.custom_id else ''})"

    class Meta:
        unique_together = ('name', 'model',)


class Device(models.Model):
    controller = models.CharField(max_length=128)
    description = models.TextField(
        blank=True, null=True)
    sensors = models.ManyToManyField(
        Sensor, blank=True, related_name="devices")
    actuators = models.ManyToManyField(
        Actuator, blank=True, related_name="devices")

    def __str__(self):
        return f"{self.controller} - {self.id}"

    def __unicode__(self):
        return f"{self.controller} - {self.id}"

    def __repr__(self):
        return f"{self.controller} - {self.id}"


class Node(models.Model):
    collection = models.ForeignKey('Collection', on_delete=models.CASCADE,
                                   blank=True, related_name="node")
    name = models.CharField(max_length=512,
                            unique=True, validators=[validate_name])
    label = models.ManyToManyField(Label, blank=True, related_name="nodes")
    device = models.ForeignKey(
        'Device', on_delete=models.CASCADE, related_name="node")
    location = PlainLocationField(
        based_fields=['city'], zoom=7, default='17.44642,78.3481')
    elevation = models.PositiveBigIntegerField(default=0)
    description = models.TextField(blank=True, null=True)
    ip_address = models.CharField(max_length=128, blank=True, null=True)
    visibility = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name}{f'- {self.ip_address}' if self.ip_address else ''}"

    def __unicode__(self):
        return f"{self.name}{f'- {self.ip_address}' if self.ip_address else ''}"

    def __repr__(self):
        return f"{self.name}{f'- {self.ip_address}' if self.ip_address else ''}"

    class Meta:
        verbose_name_plural = "Nodes"


class Onem2mInstance(models.Model):
    name = models.CharField(max_length=512, default="Local Development")
    host_address = models.CharField(max_length=512, default="localhost")
    host_port = models.PositiveBigIntegerField(default=8080)
    https = models.BooleanField(default=False)
    verify = models.BooleanField(default=True)
    base_infra = models.CharField(max_length=512, default="/~/in-cse/in-name/")
    # Need to support MN

    @property
    def link(self):
        return f"{'https' if self.https else 'http'}://{self.host_address}:{self.host_port}{self.base_infra}"

    def __str__(self):
        return f"{self.name} - {self.link}"

    def __unicode__(self):
        return f"{self.name} - {self.link}"

    def __repr__(self):
        return f"{self.name} - {self.link}"


class Collection(models.Model):
    om2m_instance = models.ForeignKey(
        'Onem2mInstance', on_delete=models.PROTECT, related_name="collection", blank=True)
    name = models.CharField(max_length=512, unique=True,
                            validators=[validate_name])
    label = models.ManyToManyField(
        Label, related_name="collection", blank=True)
    access_control_id = models.CharField(max_length=512, null=True, blank=True)
    max_instance_count = models.PositiveBigIntegerField(default=120)

    def __str__(self):
        return f"{self.name} - {self.om2m_instance.name}"

    def __unicode__(self):
        return f"{self.name} - {self.om2m_instance.name}"

    def __repr__(self):
        return f"{self.name} - {self.om2m_instance.name}"
